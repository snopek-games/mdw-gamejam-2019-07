extends Node2D

var vector = null
var speed = 100

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if vector:
		position += vector * speed * delta

func _on_Area2D_body_entered(body):
	queue_free()
