extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

var speed = 100
var vector

func _state_enter(info : Dictionary):
	vector = info['collision_normal']
	host.get_node("AnimationPlayer").play("hit")
	$HitTimer.start()

func _physics_process(delta):
	host.move_and_slide(vector * speed)

func _on_HitTimer_timeout():
	if host.hp == 0:
		get_parent().change_state("Dead")
	else:
		get_parent().change_state("Idle")

	