extends "res://Player/Idle.gd"

#onready var host = $"../.."

func _state_enter(info : Dictionary):
	host.get_node("AnimationPlayer").play("run")

func _physics_process(delta):
	var speed = 50
	var vector = _get_player_input_vector()
	if vector.length() == 0:
		get_parent().change_state("Idle")
	else:
		if vector.x > 0:
			host.get_node("Sprite").flip_h = false
		if vector.x < 0:
			host.get_node("Sprite").flip_h = true
		
		host.move_and_slide(vector * speed)

