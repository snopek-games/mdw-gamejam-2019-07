extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info : Dictionary):
	var animation_player = host.get_node("AnimationPlayer")
	animation_player.play("die")
	host.get_node("Shadow").visible = false
	yield(animation_player, "animation_finished")
	host.emit_signal("die")
