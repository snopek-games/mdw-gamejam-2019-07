extends "res://addons/snopek_state_machine/State.gd"

onready var host = $"../.."

func _state_enter(info : Dictionary):
	host.get_node('AnimationPlayer').play("idle")

func _get_player_input_vector():
	var vector = Vector2()
	if Input.is_action_pressed("player_down"):
		vector.y += 1
	if Input.is_action_pressed("player_up"):
		vector.y -= 1
	if Input.is_action_pressed("player_right"):
		vector.x += 1
	if Input.is_action_pressed("player_left"):
		vector.x -= 1
	return vector.normalized()

func _physics_process(delta):
	var vector = _get_player_input_vector()
	if vector.length() > 0:
		get_parent().change_state("Walking")
