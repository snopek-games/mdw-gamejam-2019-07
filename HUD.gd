extends CanvasLayer

# Called when the node enters the scene tree for the first time.
func _ready():
	new_game()

func new_game():
	$Heart.frame = 0
	$GameOver.visible = false
	$RetryButton.visible = false

func _on_Player_hit(hp):
	$Heart.frame = 2 - hp

func _on_Player_die():
	$GameOver.visible = true
	$RetryButton.visible = true

func _on_Enemy_die():
	$GameOver.text = "You Win!"
	$GameOver.visible = true
	$RetryButton.visible = true

func _on_RetryButton_pressed():
	get_tree().reload_current_scene()
