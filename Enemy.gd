extends KinematicBody2D

signal die

enum AI_MODE { WAIT, CHASE, HIT, DEAD }
var mode = AI_MODE.WAIT

var speed = 50
var vector = Vector2()
var hp = 2

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("idle")

func _physics_process(delta):
	if mode == AI_MODE.DEAD:
		return
	
	# Chase the player
	var player : KinematicBody2D = $"../Player"
	if player != null:
		if mode == AI_MODE.CHASE:
			vector = Vector2()
			if player.position.y < position.y:
				vector.y -= 1
			elif player.position.y > position.y:
				vector.y += 1
			if player.position.x < position.x:
				vector.x -= 1
				$Sprite.flip_h = true
			elif player.position.x > position.x:
				vector.x += 1
				$Sprite.flip_h = false
			
			$AnimationPlayer.play("run")
	
		if mode == AI_MODE.WAIT || !vector || vector.length() == 0:
			$AnimationPlayer.play("idle")
			return
		
		if mode == AI_MODE.HIT:
			$AnimationPlayer.play("hit")
		
		move_and_slide(vector.normalized() * speed)

func _on_Timer_timeout():
	if mode == AI_MODE.WAIT || mode == AI_MODE.HIT:
		mode = AI_MODE.CHASE
	else:
		mode = AI_MODE.WAIT

func _on_Hitbox_area_entered(area : Area2D):
	if mode != AI_MODE.HIT && mode != AI_MODE.DEAD && area.get_collision_layer_bit(3):
		hp -= 1
		mode = AI_MODE.HIT
		vector = position - area.position

		$AnimationPlayer.play("hit")

		print(hp)

		if hp <= 0:
			$Timer.stop()
			mode = AI_MODE.DEAD
			$AnimationPlayer.get_animation("hit").loop = false
			yield($AnimationPlayer, "animation_finished")
			visible = false
			yield(get_tree().create_timer(1.0), "timeout")
			emit_signal("die")
			queue_free()
			

