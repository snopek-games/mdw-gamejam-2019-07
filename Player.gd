extends KinematicBody2D

signal die
signal hit

var Magic = preload("res://Magic.tscn")

var hp = 2
var speed = 50
var vector = Vector2()
var staff_vector = Vector2()
	
func _on_Hitbox_body_entered(body : PhysicsBody2D):
	# @todo Make this fit in better with the state pattern
	if body && $States.current_state.name in ["Idle", "Walking"] && body.get_collision_layer_bit(1):
		var collision_normal = (position - body.position).normalized()
		$States.change_state("Hit", {'collision_normal': collision_normal})
		
		hp -= 1
		emit_signal("hit", hp)

func _process(delta):
	staff_vector = get_global_mouse_position() - global_position
	$Staff.rotation = staff_vector.angle()
	
	if $States.current_state.name != "Dead" && get_parent() && Input.is_action_just_released("player_shoot"):
		var magic = Magic.instance()
		magic.vector = staff_vector.normalized()
		magic.position = position + $Staff.position + (magic.vector * 16)
		get_parent().add_child(magic)


