tool
extends Node

var current_state

# @todo Causes some error in game!
#func _get_configuration_warning() -> String:
#	var bad_children = PoolStringArray()
#	for child in get_children():
#		if not child is preload("res://addons/snopek_state_machine/State.gd"):
#			bad_children.append(child.name)
#	if bad_children.size() > 0:
#		return "All direct children of StateMachine must be State: " + bad_children.join(", ")
#	return ""

func _ready():
	if get_child_count() > 0:
		change_state(get_children()[0].name)

func change_state(name : String, info : Dictionary = {}):
	var next_state = get_node(name)
	if next_state == null:
		return

	if current_state:
		if current_state.has_method('_state_exit'):
			current_state._state_exit()
		
	# Disable processing for all children
	for child in get_children():
		child.set_process_input(false)
		child.set_process_unhandled_input(false)
		child.set_process_unhandled_key_input(false)
		child.set_process(false)
		child.set_physics_process(false)
	
	
	var previous_state = current_state
	current_state = next_state
	
	# Re-enable processing for the current state
	if current_state.has_method('_input'):
		current_state.set_process_input(true)
	if current_state.has_method('_unhandled_input'):
		current_state.set_process_unhandled_input(true)
	if current_state.has_method('_unhandled_key_input'):
		current_state.set_process_unhandled_key_input(true)
	if current_state.has_method('_process'):
		current_state.set_process(true)
	if current_state.has_method('_physics_process'):
		current_state.set_physics_process(true)	
	
	if current_state != previous_state:
		if current_state.has_method('_state_enter'):
			current_state._state_enter(info)

